TP Réseau 3

# I. ARP

## 1. Echange ARP

### Générer des requêtes ARP

Effectuer un `ping` d'une machine à l'autre

Depuis **john** :
```
$ ping 10.3.1.12

PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=0.527 ms
64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=0.451 ms
^C
--- 10.3.1.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1023ms
rtt min/avg/max/mdev = 0.451/0.489/0.527/0.038 ms
```

Depuis **marcel** :
```
$ ping 10.3.1.11

PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=64 time=0.767 ms
64 bytes from 10.3.1.11: icmp_seq=2 ttl=64 time=0.341 ms
64 bytes from 10.3.1.11: icmp_seq=3 ttl=64 time=0.276 ms
64 bytes from 10.3.1.11: icmp_seq=4 ttl=64 time=0.406 ms
64 bytes from 10.3.1.11: icmp_seq=5 ttl=64 time=0.474 ms
^C
--- 10.3.1.11 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4114ms
rtt min/avg/max/mdev = 0.276/0.452/0.767/0.170 ms
```

### Observer les tables ARP des deux machines

**John** :
```
$ ip neigh show

10.3.1.12 dev enp0s8 lladdr 08:00:27:9d:26:d2 REACHABLE
10.3.1.10 dev enp0s8 lladdr 0a:00:27:00:00:13 REACHABLE
10.0.2.2 dev enp0s3 lladdr 52:54:00:12:35:02 REACHABLE
```

**Marcel** :
```
$ ip neigh show

10.3.1.10 dev enp0s8 lladdr 0a:00:27:00:00:13 DELAY
10.3.1.11 dev enp0s8 lladdr 08:00:27:1d:3b:74 STALE
10.0.2.2 dev enp0s3 lladdr 52:54:00:12:35:02 STALE
```

Mac de **John** : `08:00:27:1d:3b:74`
Mac de **Marcel** : `08:00:27:9d:26:d2`

### Prouvez que l'info est correcte

+ Une commande pour voir la MAC de **Marcel** dans la table ARP de **John** :

```
$ ip neigh show 10.3.1.12

10.3.1.12 dev enp0s8 lladdr 08:00:27:9d:26:d2 STALE
```

*C'est la même*

+ Une commande pour afficher la MAC de **Marcel** depuis **Marcel** :
```
$ ip a | grep link/ether
	...
    link/ether 08:00:27:9d:26:d2 brd ff:ff:ff:ff:ff:ff
```

La MAC de **marcel** est toujours : **08:00:27:9d:26:d2**

*C'est carré*

## 2. Analyse de trames

### Utilisez la commande `tcpdump` pour réaliser une capture de trame

```
$ sudo tcpdump

dropped privs to tcpdump
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on enp0s3, link-type EN10MB (Ethernet), snapshot length 262144 bytes
10:35:40.396648 ARP, Request who-has _gateway tell localhost.localdomain, length 28
10:35:40.396796 ARP, Reply _gateway is-at 52:54:00:12:35:02 (oui Unknown), length 46
10:35:40.396799 IP localhost.localdomain.59016 > 51-15-175-180.rev.poneytelecom.eu.ntp: NTPv4, Client, length 48
10:35:40.404280 IP localhost.localdomain.48979 > dns.google.domain: 37322+ PTR? 2.2.0.10.in-addr.arpa. (39)
10:35:40.420983 IP 51-15-175-180.rev.poneytelecom.eu.ntp > localhost.localdomain.59016: NTPv4, Server, length 48
10:35:40.431049 IP dns.google.domain > localhost.localdomain.48979: 37322 NXDomain 0/0/0 (39)
10:35:40.431305 IP localhost.localdomain.48728 > dns.google.domain: 64594+ PTR? 15.2.0.10.in-addr.arpa. (40)
10:35:40.460466 IP dns.google.domain > localhost.localdomain.48728: 64594 NXDomain 0/0/0 (40)
10:35:40.461009 IP localhost.localdomain.45229 > dns.google.domain: 33761+ PTR? 180.175.15.51.in-addr.arpa. (44)
10:35:40.486123 IP dns.google.domain > localhost.localdomain.45229: 33761 1/0/0 PTR 51-15-175-180.rev.poneytelecom.eu. (91)
10:35:40.507380 IP localhost.localdomain.52005 > dns.google.domain: 10048+ PTR? 8.8.8.8.in-addr.arpa. (38)
10:35:40.532541 IP dns.google.domain > localhost.localdomain.52005: 10048 1/0/0 PTR dns.google. (62)
10:35:41.648576 IP localhost.localdomain.50295 > y.ns.gin.ntt.net.ntp: NTPv4, Client, length 48
10:35:41.651587 IP localhost.localdomain.59485 > dns.google.domain: 26342+ PTR? 251.35.250.129.in-addr.arpa. (45)
10:35:41.669793 IP y.ns.gin.ntt.net.ntp > localhost.localdomain.50295: NTPv4, Server, length 48
10:35:41.671988 IP dns.google.domain > localhost.localdomain.59485: 26342 1/0/0 PTR y.ns.gin.ntt.net. (75)
```

Les deux élus sont ici :

```
10:35:40.396648 ARP, Request who-has _gateway tell localhost.localdomain, length 28
10:35:40.396796 ARP, Reply _gateway is-at 52:54:00:12:35:02 (oui Unknown), length 46
```

Les paquets sont [ici](pcaps/tp3_arp.pcapng).

# II. Routage

## 1. Mise en place du routage

### Activer le routage sur le noeud `router`

```
$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s3 enp0s8 enp0s9
[chval@localhost ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[chval@localhost ~]$ sudo firewall-cmd --add-masquerade --zone=public -
-permanent
success
```

### Ajouter les routes statiques nécessaires pour que `john` et `marcel` puissent se `ping`

Les routes de **marcel** : 
```
$ ip route show

10.3.0.0/22 dev enp0s8 proto kernel scope link src 10.3.2.12 metric 100
10.3.1.0 via 10.3.2.254 dev enp0s8 proto static metric 100
```

Les routes de **john** : 
```
$ ip route show

10.3.0.0/22 dev enp0s8 proto kernel scope link src 10.3.1.11 metric 100
10.3.0.0/22 via 10.3.1.254 dev enp0s8 proto static metric 100
```

Et un petit ping de chaque côté : 

**Marcel** -> **John** :
```
$ ping 10.3.1.11

PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=64 time=0.297 ms
64 bytes from 10.3.1.11: icmp_seq=2 ttl=64 time=0.278 ms
64 bytes from 10.3.1.11: icmp_seq=3 ttl=64 time=0.188 ms
^C
--- 10.3.1.11 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2026ms
rtt min/avg/max/mdev = 0.188/0.254/0.297/0.047 ms
```

**John** -> **Marcel** :
```
$ ping 10.3.2.12

PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=64 time=0.367 ms
64 bytes from 10.3.2.12: icmp_seq=2 ttl=64 time=0.417 ms
64 bytes from 10.3.2.12: icmp_seq=3 ttl=64 time=0.197 ms
64 bytes from 10.3.2.12: icmp_seq=4 ttl=64 time=0.177 ms
64 bytes from 10.3.2.12: icmp_seq=5 ttl=64 time=0.219 ms
^C
--- 10.3.2.12 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4101ms
rtt min/avg/max/mdev = 0.177/0.275/0.417/0.097 ms
```

## 2. Analyse de trames

### Analyse des échanges ARP

J'ai vidé les tables des trois VMs en utilisant la commande :

```
$ sudo ip neigh flush all
```

Ping de **John** vers **Marcel** :

```
$ ping 10.3.2.12

PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=64 time=0.397 ms
64 bytes from 10.3.2.12: icmp_seq=2 ttl=64 time=0.351 ms
64 bytes from 10.3.2.12: icmp_seq=3 ttl=64 time=0.363 ms
64 bytes from 10.3.2.12: icmp_seq=4 ttl=64 time=0.224 ms
^C
--- 10.3.2.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3100ms
rtt min/avg/max/mdev = 0.224/0.333/0.397/0.065 ms
```

> Le dump de paquets est [ici](pcaps/tp3_routage_marcel.pcapng).

| ordre | type trame  | IP source | MAC source              | IP destination | MAC destination            |
|-------|-------------|-----------|-------------------------|----------------|----------------------------|
| 1     | Requête ARP | x         | `marcel PcsCompu_1d:3b:74` | x              | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | x         | `marcel PcsCompu_9d:26:d2`                       | x              | `marcel PcsCompu_1d:3b:74`    |      |
| 3 | Ping | 10.3.1.11 | x | 10.3.2.12 | x |
| 4 | Pong | 10.3.2.12 | x | 10.3.1.11 | x |
| 5 | Ping | 10.3.1.11 | x | 10.3.2.12 | x |
| 6 | Pong | 10.3.2.12 | x | 10.3.1.11 | x |
| 7 | Ping | 10.3.1.11 | x | 10.3.2.12 | x |
| 8 | Pong | 10.3.2.12 | x | 10.3.1.11 | x |
| 9 | Ping | 10.3.1.11 | x | 10.3.2.12 | x |
| 10 | Pong | 10.3.2.12 | x | 10.3.1.11 | x |

## 3. Accès internet

### Donnez un accès internet à vos machines

J'ai rajouté une route par défaut

Je ping **le DNS de Google** (8.8.8.8) depuis **Marcel** :

```
$ ping 8.8.8.8

PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=26.6 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=112 time=24.7 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=112 time=21.7 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=112 time=23.8 ms
64 bytes from 8.8.8.8: icmp_seq=5 ttl=112 time=21.8 ms
64 bytes from 8.8.8.8: icmp_seq=6 ttl=112 time=22.1 ms
64 bytes from 8.8.8.8: icmp_seq=7 ttl=112 time=24.8 ms
64 bytes from 8.8.8.8: icmp_seq=8 ttl=112 time=25.1 ms
^C
--- 8.8.8.8 ping statistics ---
8 packets transmitted, 8 received, 0% packet loss, time 7016ms
rtt min/avg/max/mdev = 21.745/23.812/26.570/1.669 ms
```

Puis depuis **John** :

```
$ ping 8.8.8.8

PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=23.7 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=112 time=23.0 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=112 time=21.4 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=112 time=22.2 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 21.371/22.558/23.654/0.849 ms
```

Ils ont accès à Internet *ces fourbes*.

![internet](https://c.tenor.com/s1LAPOXya-wAAAAd/internet-bravo.gif)
*Bienvenue sur Internet*

### Donnez leur aussi l'adresse d'un serveur DNS qu'ils peuvent utiliser

*Curieusement la résolution DNS marche déjà, NetworkManager l'a déjà setup.*

```
$ sudo cat /etc/resolv.conf

# Generated by NetworkManager
nameserver 8.8.8.8
nameserver 8.8.4.4
nameserver 1.1.1.1
```

Test du bon fonctionnement :

```
$ curl gitlab.com
(retourne rien = OK)
```

Requête direct en DNS avec **dig** :

```
$ dig gitlab.com

*
; <<>> DiG 9.16.23-RH <<>> gitlab.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 46576
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;gitlab.com.                    IN      A

;; ANSWER SECTION:
gitlab.com.             300     IN      A       172.65.251.78

;; Query time: 39 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Fri Oct 28 10:47:52 CEST 2022
;; MSG SIZE  rcvd: 55
```

(ça marche)

Le ping :

```
$ ping google.com
PING google.com (142.250.179.110) 56(84) bytes of data.
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=1 ttl=247 time=21.1 ms
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=2 ttl=247 time=20.3 ms
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=3 ttl=247 time=21.1 ms
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=4 ttl=247 time=21.2 ms
^C
--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 20.254/20.906/21.246/0.383 ms
```

### Analyse de trames

Effectuez un ping de `8.8.8.8` depuis **John** :

Paquets [ici](pcaps/tp3_routage_marcel.pcapng)

Analysez un ping aller et le retour qui correspond et mettez dans un tableau :

| ordre | type trame | IP source          | MAC source              | IP destination | MAC destination |
|-------|------------|--------------------|-------------------------|----------------|-----------------|
| 1     | ping       | 10.3.1.11 | x | 8.8.8.8     | x               |     |
| 2     | pong       | 8.8.8.8                | x                     | 10.3.1.11            | x             |

# III. DHCP

## Mise en place du serveur DHCP

### Sur la machine John, vous installerez et configurerez un serveur DHCP

Fichier de configuration `/etc/dhcp/dhcpd.conf` :

```
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#

default-lease-time 900;
max-lease-time 10800;

authoritative;

subnet 10.3.1.0 netmask 255.255.255.0 {
        range 10.3.1.1 10.3.1.253;
        option routers 10.3.1.254;
        option subnet-mask 255.255.255.0;
}
```

On demande à **Bob** d'utiliser le serveur DHCP de **John** :

Fichier `/etc/sysconfig/network-scripts/ifcfg-enp0s8` :

```
DEVICE=enp0s8
BOOTPROTO=dhcp
ONBOOT=yes
```

On redémarre et **Bob** a bien une nouvelle adresse IP délivrée par le DHCP : **10.3.1.1**

### Améliorer la configuration du serveur DHCP

Ajoutez de la configuration à votre DHCP pour qu'il donne aux clients, en plus de leur IP : une route par défaut, et un serveur DNS à utiliser.

On modifie le fichier de config `/etc/dhcp/dhcpd.conf` :

```
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#

default-lease-time 900;
max-lease-time 10800;

authoritative;

subnet 10.3.1.0 netmask 255.255.255.0 {
        range 10.3.1.1 10.3.1.253;
        option routers 10.3.1.254;
        option subnet-mask 255.255.255.0;
        option domain-name-servers 8.8.8.8;
}
```

On rajoute une route par défaut : 

```
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#

default-lease-time 900;
max-lease-time 10800;

authoritative;

subnet 0.0.0.0 netmask 255.255.255.0 {
        range 10.3.1.1 10.3.2.253;
        option routers 10.3.1.254;
        option subnet-mask 255.255.255.0;
        option domain-name-servers 8.8.8.8;
}

subnet 10.3.1.0 netmask 255.255.255.0 {
        range 10.3.1.1 10.3.1.253;
        option routers 10.3.1.254;
        option subnet-mask 255.255.255.0;
        option domain-name-servers 8.8.8.8;
}
```

On rajoute un serveur DNS (**8.8.8.8**, celui de Google) :

```
option domain-name-servers 8.8.8.8;
```

Récupérez de nouveau une IP en DHCP sur **Bob** pour tester :

```
$ sudo dhclient -r
```
*On renew le bail DHCP*

Vérifier avec une commande qu'il a récupéré son IP :

```
$ ip a

1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 08:00:27:9d:59:6b brd ff:ff:ff:ff:ff:ff
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:22:c3:67 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.1/24 brd 10.3.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 483sec preferred_lft 483sec
    inet6 fe80::a00:27ff:fe22:c367/64 scope link
       valid_lft forever preferred_lft forever
```
L'IP de **Bob** est **10.3.1.1** (la première IP disponible sur le serveur DHCP).

Avec `dig`, on voit que **Bob** utilise bien le DNS de Google :

```
$ dig google.com

; <<>> DiG 9.16.23-RH <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 23389
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             300     IN      A       142.250.179.78

;; Query time: 28 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Fri Oct 28 12:52:35 CEST 2022
;; MSG SIZE  rcvd: 55
```
*en résolvant Google*

Vérifier qu'il peut `ping` sa passerelle

```
$ ping 10.3.1.254

PING 10.3.1.254 (10.3.1.254) 56(84) bytes of data.
64 bytes from 10.3.1.254: icmp_seq=1 ttl=64 time=0.261 ms
64 bytes from 10.3.1.254: icmp_seq=2 ttl=64 time=0.314 ms
64 bytes from 10.3.1.254: icmp_seq=3 ttl=64 time=0.340 ms
64 bytes from 10.3.1.254: icmp_seq=4 ttl=64 time=0.301 ms
^C
--- 10.3.1.254 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3077ms
rtt min/avg/max/mdev = 0.261/0.304/0.340/0.028 ms
```
*Il le peut*

Vérifier la présence de la route (par défaut) avec une commande :

```
$ ip route show

default via 10.3.1.254 dev enp0s8 proto dhcp src 10.3.1.1 metric 100
10.3.1.0/24 dev enp0s8 proto kernel scope link src 10.3.1.1 metric 100
```
*Elle est là*

Vérifier que la route fonctionne avec un `ping` vers une IP :

```
$ ping 8.8.8.8

PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=21.9 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=112 time=21.6 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=112 time=23.0 ms
^C
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 21.574/22.140/22.960/0.593 ms
```

Vérifier avec la commande `dig` qu'il peut communiquer avec un DNS : 

```
$ dig google.com

; <<>> DiG 9.16.23-RH <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 23389
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             300     IN      A       142.250.179.78

;; Query time: 28 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Fri Oct 28 12:52:35 CEST 2022
;; MSG SIZE  rcvd: 55
```

Vérifier un `ping` vers un nom de domaine : 

```
$ ping google.com
PING google.com (142.250.74.238) 56(84) bytes of data.
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=1 ttl=247 time=20.7 ms
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=2 ttl=247 time=20.7 ms
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=3 ttl=247 time=20.7 ms
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=4 ttl=247 time=20.9 ms
^C
--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 20.676/20.744/20.863/0.070 ms
```

## Analyse de trames

Je commence à capturer les paquets puis je demande une nouvelle IP avec `dhclient` :

```
$ sudo dhclient -r
$ sudo dhclient
```

### Repérez, dans les trames DHCP observées dans Wireshark, les infos que votre serveur a fourni au client

+ L'IP fournie au client : **10.3.1.101**
+ L'IP de la passerelle : **10.3.1.254**
+ L'IP du DNS fourni : **8.8.8.8** (Google)

Les paquets sont [ici](pcaps/tp3_dhcp.pcapng) (Paquets 15 à 19, en ignorant le paquet 17 *qui se prend un gros vent*).