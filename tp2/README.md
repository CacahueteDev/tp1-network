TP Réseau 2

## I. Setup IP

### Mettez en place une configuration réseau fonctionnelle entre les deux machines

J'ai utilisé `netsh` pour set l'IP depuis la ligne de commande :

Commande : `netsh`

```
netsh>interface ip
netsh interface ipv4>set address "Ethernet" static 10.69.1.12 255.255.252.0 192.168.1.1
```

Voici les infos :

|Nom|Valeur|
|---------------|-----------|
|Mon adresse IP |10.69.1.12|
|L'autre adresse IP |10.69.1.11|
|Adresse de réseau |10.69.0.0|
|Adresse de broadcast|10.69.3.255|

### Prouvez que la connexion est fonctionnelle entre les deux machines

Commande : `ping 10.69.1.11`

```
Envoi d’une requête 'Ping'  10.69.1.11 avec 32 octets de données :
Réponse de 10.69.1.11 : octets=32 temps<1ms TTL=64
Réponse de 10.69.1.11 : octets=32 temps<1ms TTL=64
Réponse de 10.69.1.11 : octets=32 temps<1ms TTL=64
Réponse de 10.69.1.11 : octets=32 temps<1ms TTL=64

Statistiques Ping pour 10.69.1.11:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
```

### Wireshark it

[Le lien du PCAP](Dumps/ping.pcapng)

### Déterminez, grâce à Wireshark, quel type de paquet ICMP est envoyé par ping

Le type de packet pour le ping (aller) est un packet **ICMP** de **type 8**

Le type de packet pour le pong (retour) est un packet **ICMP** de **type 0**

### Check the ARP table

```
Interface : 10.69.1.12 --- 0x14
  Adresse Internet      Adresse physique      Type
  10.69.1.11            f8-e4-3b-bd-7e-c0     dynamique
  10.69.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```

### Déterminez la MAC de votre binome depuis votre table ARP

Adresse MAC de mon binome : `f8-e4-3b-bd-7e-c0`

### Déterminez la MAC de la gateway de votre réseau

À l'aide de la commande `arp -a`, j'ai eu comme résultat : 

```
Interface : 10.33.17.11 --- 0x4
  Adresse Internet      Adresse physique      Type
  10.33.18.221          78-4f-43-87-f5-11     dynamique
  10.33.19.254          00-c0-e7-e0-04-4e     dynamique
  10.33.19.255          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```

L'adresse MAC du gateway est `00-c0-e7-e0-04-4e`

### Manipuler la table ARP

### Utilisez une commande pour vider votre table ARP

J'ai utilisé la commande `arp -d` pour vider ma table ARP

### Prouvez que ça fonctionne en l'affichant et en constatant les changements

```
arp -a

Interface : 10.33.17.11 --- 0x4
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique

Interface : 192.168.229.1 --- 0xc
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique

Interface : 10.69.1.12 --- 0x14
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.56.1 --- 0x15
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.211.1 --- 0x1a
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique
```

La table est quasiment vide (pas totalement, *Windows ?*)

J'ai ensuite re-ping le mate, et ensuite j'ai fais un `arp -a` : 

```
arp -a

Interface : 10.33.17.11 --- 0x4
  Adresse Internet      Adresse physique      Type
  10.33.19.254          00-c0-e7-e0-04-4e     dynamique
  10.33.19.255          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.229.1 --- 0xc
  Adresse Internet      Adresse physique      Type
  192.168.229.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.69.1.12 --- 0x14
  Adresse Internet      Adresse physique      Type
  10.69.1.11            f8-e4-3b-bd-7e-c0     dynamique
  10.69.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.56.1 --- 0x15
  Adresse Internet      Adresse physique      Type
  192.168.56.255        ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.211.1 --- 0x1a
  Adresse Internet      Adresse physique      Type
  192.168.211.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```

Des informations supplémentaires sont maintenant affichées

### Wireshark it

### Mettez en évidence les deux trames ARP échangées lorsque vous essayez de contacter quelqu'un pour la "première" fois

J'ai vidé la table ARP sur Windows avec la commande `arp -d`

Ensuite, j'ai ping le routeur `10.33.19.254` et j'ai pu capturer [ces paquets](Dumps/arp.pcapng).

La table ARP s'est bien modfiée : 

```
arp -a

Interface : 10.33.17.11 --- 0x4
  Adresse Internet      Adresse physique      Type
  10.33.19.254          00-c0-e7-e0-04-4e     dynamique
  10.33.19.255          ff-ff-ff-ff-ff-ff     statique
  224.0.0.2             01-00-5e-00-00-02     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique

Interface : 192.168.229.1 --- 0xb
  Adresse Internet      Adresse physique      Type
  192.168.229.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique

Interface : 10.69.1.12 --- 0x12
  Adresse Internet      Adresse physique      Type
  10.69.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique

Interface : 192.168.56.1 --- 0x13
  Adresse Internet      Adresse physique      Type
  192.168.56.255        ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.211.1 --- 0x16
  Adresse Internet      Adresse physique      Type
  192.168.211.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique

Interface : 172.26.224.1 --- 0x43
  Adresse Internet      Adresse physique      Type
  172.26.239.255        ff-ff-ff-ff-ff-ff     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
```

### Déterminez, pour les deux trames, les adresses source et destination

**Paquet 1 : ARP Broadcast**
- Source : `Fiberdat_e0:04:4e`
- Destination : `IntelCor_f5:18:f6`

**Paquet 2 : ARP Reply**
- Source : `IntelCor_f5:18:f6`
- Destination : `Fiberdat_e0:04:4e`

Ces adresses correspondent aux adresses MAC des cartes réseaux de mon PC (`IntelCor_f5:18:f6`) ainsi que du routeur (`Fiberdat_e0:04:4e`)

## III. DHCP you too my brooo

![Dora frère](https://media.discordapp.net/attachments/741645926447317123/1028932881243971624/unknown.png)

**DORA** = *Discover Offer Request Acknowledge*

*D'où la vanne*

### Wireshark it

### Identifiez les 4 trames DHCP lors d'un échange DHCP

On peut voir les quatres trames DHCP *DORA* [ici](Dumps/dora_explorer.pcapng).

**Paquets 1 et 3**
- Source : `0.0.0.0` (C'est une adresse IP spéciale, elle est généralement utilisée pour dire qu'on écoute sur toutes les interfaces réseau sur un système)
- Destination : `255.255.255.255` (Adresse de broadcast : le PC communique avec tout le monde)

**Paquets 2 et 4**
- Source : `10.33.19.254` (L'adresse du routeur)
- Destination : `10.33.17.11` (Mon adresse IP supposée : je ne l'ai pas vraiment à ce moment là, c'est *Wireshark* qui le sous-entend)

### Identifiez dans ces 4 trames les informations 1, 2 et 3 dont on a parlé juste au dessus

1. L'adresse IP à utiliser : `10.33.17.11`
2. L'adresse IP de la passerelle du réseau : `10.33.19.254`
3. L'adresse d'un serveur DNS joignable depuis le réseau : `8.8.8.8` (le DNS de *Google*)