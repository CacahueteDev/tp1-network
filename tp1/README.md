# TP réseau

## I. Exploration locale en solo

## 1. Affichage d'informations sur la pile TCP/IP

### Affichez les infos des cartes réseau de votre PC

**Carte wifi**

Commande : `ipconfig /all`

| Nom | Adresse MAC | Adresse IP |
|-----|-------------|------------|
|Carte réseau sans fil Wi-Fi | 84-5C-F3-F5-18-F6 | 10.33.16.221 |

Résultat : 
```
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6E AX210 160MHz
   Adresse physique . . . . . . . . . . . : 84-5C-F3-F5-18-F6
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::894b:b9a4:23fa:f93%4(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.16.221(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Bail obtenu. . . . . . . . . . . . . . : lundi 3 octobre 2022 09:00:31
   Bail expirant. . . . . . . . . . . . . : mardi 4 octobre 2022 09:00:29
   Passerelle par défaut. . . . . . . . . : 10.33.19.254
   Serveur DHCP . . . . . . . . . . . . . : 10.33.19.254
   IAID DHCPv6 . . . . . . . . . . . : 59006195
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-27-D0-A1-9C-D8-BB-C1-1E-AE-FA
   Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
                                       8.8.4.4
                                       1.1.1.1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
```

**Carte Ethernet**

Commande : `ipconfig /all`

| Nom | Adresse MAC | Adresse IP |
|-----|-------------|------------|
|Carte Ethernet Ethernet | D8-BB-C1-1E-AE-FA | N/A |

Résultat :
```
Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Killer E3100X 2.5 Gigabit Ethernet Controller (2)
   Adresse physique . . . . . . . . . . . : D8-BB-C1-1E-AE-FA
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::75e7:38e8:aa7c:3b73%26(préféré)
   Adresse d’autoconfiguration IPv4 . . . : 169.254.59.115(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.0.0
   Passerelle par défaut. . . . . . . . . :
   IAID DHCPv6 . . . . . . . . . . . : 148421569
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-27-D0-A1-9C-D8-BB-C1-1E-AE-FA
   Serveurs DNS. . .  . . . . . . . . . . : 192.168.0.254
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
```

### Affichez votre gateway

Commande : `ipconfig /all`

Adresse du gateway : `10.33.19.254`

Extrait de la ligne complète : 
```
Passerelle par défaut. . . . . . . . . : 10.33.19.254
```

### Déterminer la MAC de la passerelle

Sortie de la commande `arp -a` (uniquement la bonne interface) a.k.a. *la table ARP* :
```
Interface : 10.33.16.221 --- 0x4
  Adresse Internet      Adresse physique      Type
  10.33.19.254          00-c0-e7-e0-04-4e     dynamique
  10.33.19.255          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```

La ligne intéressante :
```
10.33.19.254          00-c0-e7-e0-04-4e     dynamique
```

Adresse MAC de la passerelle : `00-c0-e7-e0-04-4e`

## Partie GUI

###  Trouvez comment afficher les informations sur une carte IP (change selon l'OS)

Screenshots :
![Une image](https://media.discordapp.net/attachments/741645926447317123/1026422801214734396/unknown.png?width=942&height=663)

![L'obscurité](https://media.discordapp.net/attachments/741645926447317123/1026423433988411462/unknown.png)

Données trouvées :

| Adresse IP | Adresse MAC | Passerelle |
|-----|-------------|------------|
| 10.33.16.221 | 84-5C-F3-F5-18-F6 | 10.33.19.254 |

## 2. Modifications des informations

## A. Modification d'adresse IP (part 1)

### Utilisez l'interface graphique de votre OS pour changer d'adresse IP :

Pour changer l'adresse IP, je suis allé dans `Paramètres Wi-Fi` :

![Une image qui s'affiche tkt](https://media.discordapp.net/attachments/741645926447317123/1026422801214734396/unknown.png?width=942&height=663)

Attribution d'adresse IP, je clique sur `Modifier`

![Une image qui s'affiche tkt](https://media.discordapp.net/attachments/741645926447317123/1026426810503790682/unknown.png)

J'ai du activer `IPv6` et re-entrer toutes les informations manuellement (merci Windows)

![](https://media.discordapp.net/attachments/741645926447317123/1026428442411008000/unknown.png?width=362&height=663)

J'ai validé et j'ai maintenant ces informations

![](https://media.discordapp.net/attachments/741645926447317123/1026429555583504424/unknown.png)

### Il est possible que vous perdiez l'accès internet. Que ce soit le cas ou non, expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération.

**Si on perd l'accès à Internet** : c'est qu'on a demandé une adresse IP locale déjà prise.

![](https://c.tenor.com/PvqrX4XSoSQAAAAd/thierry-lhermitte-thierry.gif)

*Sinon, bravo, vous êtes sur Internet !*

## II. Exploration locale en duo

On a bien branché nos PCs ensemble avec un câble Ethernet :
![](https://i.ytimg.com/vi/O0k63-FWqfw/maxresdefault.jpg)

## 3. Modification d'adresse IP

### Modifiez l'IP des deux machines pour qu'elles soient dans le même réseau

Pour modifier l'adresse IP, je suis allé dans les paramètres (encore), puis dans Réseau et Internet > Ethernet

![](https://cdn.discordapp.com/attachments/741645926447317123/1026436955178799104/unknown.png)

Puis j'ai changé l'adresse IP en `10.10.10.1` avec le masque `255.255.255.0` en laissant la passerelle *et les autres options* vides.

### Vérifier à l'aide d'une commande que votre IP a bien été changée

Je vérifie que mon adresse IP est bien changée avec (toujours la même commande) : `ipconfig /all`

Retour : 

```
Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::75e7:38e8:aa7c:3b73%26
   Adresse IPv4. . . . . . . . . . . . . .: 10.10.10.1
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . :
```

### Vérifier que les deux machines se joignent

Ping de *l'autre* machine avec la commande `ping` :
```
Envoi d’une requête 'Ping'  10.10.10.2 avec 32 octets de données :
Réponse de 10.10.10.2 : octets=32 temps<1ms TTL=64
Réponse de 10.10.10.2 : octets=32 temps<1ms TTL=64
Réponse de 10.10.10.2 : octets=32 temps<1ms TTL=64
Réponse de 10.10.10.2 : octets=32 temps<1ms TTL=64

Statistiques Ping pour 10.10.10.2:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
```

### Déterminer l'adresse MAC de votre correspondant
Je détermine l'adresse MAC de *l'autre PC* : 

Table ARP :
```
Interface : 10.10.10.1 --- 0x1a
  Adresse Internet      Adresse physique      Type
  10.10.10.2            f8-e4-3b-bd-7e-c0     dynamique
  10.10.10.255          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```

Adresse MAC de l'autre PC : `f8-e4-3b-bd-7e-c0`

## 4. Utilisation d'un des deux comme gateway

Dans ce cas-ci, mon pc était **le routeur**
![](https://image.darty.com/hifi_video/reseau/modem_routeur_wi-fi/tp-link_rt_archer_mr600_s2003104799194A_112733330.jpeg)

Sur Windows, il fallait que je change l'IP manuellement

![Une image qui s'affiche tkt](https://media.discordapp.net/attachments/741645926447317123/1026422801214734396/unknown.png?width=942&height=663)

Attribution d'adresse IP, je clique sur `Modifier`

![Une image qui s'affiche tkt](https://media.discordapp.net/attachments/741645926447317123/1026426810503790682/unknown.png)

### Déterminer l'adresse MAC de votre correspondant

L'autre PC exécute la commande `arp` :


```
% arp -a 
? (10.10.10.1) at d8:bb:c1:1e:ae:fa on en5 ifscope [ethernet]
```

### Tester l'accès Internet
Le client connecté au routeur a exécuté la commande `ping` avec l'adresse du serveur DNS `1.1.1.1` en argument
```
% ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1): 56 data bytes
64 bytes from 1.1.1.1: icmp_seq=0 ttl=54 time=22.000 ms
64 bytes from 1.1.1.1: icmp_seq=1 ttl=54 time=20.916 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=54 time=21.239 ms
...
```

### Prouver que la connexion Internet passe bien par l'autre PC

Le client connecté au routeur exécute la commande `traceroute` :

```
$ traceroute 1.1.1.1
traceroute to 1.1.1.1 (1.1.1.1), 64 hops max, 52 byte packets
 1  192.168.137.1 (192.168.137.1)  0.967 ms
```

> `192.168.137.1` est ici l'adresse IP de mon PC, le routeur, même si je l'avais changé avant, c'est Windows qui a voulu le changer (Il a eu le dernier mot).

----------

## 5. Petit chat privé
![](https://gitlab.com/it4lik/b1-reseau-2022/-/raw/main/tp/1/pics/netcat.jpg)
J'ai volé l'image désolé

### Création du serveur

> Le programme `netcat` a été considéré comme un virus par mon Windows (c'est un faux-positif, *la paranoïa de Microsoft*). J'ai donc dû temporairement désactiver cet antivirus qu'on me force d'utiliser.

Ici, je suis le **PC serveur**

J'ai entré la commande : `nc -l -p 8888`

Retour : 
```
vuyoht
bonjour
Bonjour.
Bonjours
Bonjourno
Bonjouras
Bonjournias
Bonjournias

```

Messages provenant du client : 

```
vuyoht
bonjour
```

Messages envoyés du serveur :

```
Bonjour.
Bonjours
Bonjourno
Bonjouras
Bonjournias
Bonjournias
```

### Connexion au serveur du client

Commande : `nc 192.168.137.1 8888`

On se connecte au serveur avec `netcat`.

Retour :

```
vuyoht 
bonjour 
Bonjour.
Bonjours
Bonjourno

```

### Visualiser la connexion en cours

**Coté serveur**

Commande : `netstat -a -n -b`

Retour : 
```
Connexions actives

...

TCP    192.168.137.1:8888     192.168.137.2:54320    ESTABLISHED
 [nc.exe]
TCP    192.168.211.1:139      0.0.0.0:0              LISTENING

...
```

**Coté client**

```
% netstat -a -n
Active Internet connections (including servers)
Proto Recv-Q Send-Q  Local Address          Foreign Address        (state)    
tcp4       0      0  192.168.137.2.54320    192.168.137.1.8888     ESTABLISHED
tcp4       0      0  127.0.0.1.6463         *.*                    LISTEN     
tcp4       0      0  10.33.16.254.54301     162.159.136.234.443    ESTABLISHED
tcp6       0      0  *.5000                 *.*                    LISTEN     
```

### Pour aller un peu plus loin

On peut observer ceci :


```
TCP    192.168.211.1:139      0.0.0.0:0              LISTENING
```

`0.0.0.0:0` veut dire que le serveur écoute **toutes les adresses IP**, donc toutes les interfaces. On peut donc s'y connecter en Wi-Fi.

Lancement d'un serveur `netcat` sur l'interface Ethernet uniquement

Commande : `nc -l -p 8888 -s 192.168.137.1`

Retour (message du client) : 
```
sdfghjk
```
Résultat de la commande `netstat -a -n -b` :

```
  TCP    192.168.137.1:139      0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.137.1:8888     0.0.0.0:0              LISTENING
```

## 6. Firewall

### Activer et configurer votre firewall

![](https://media.discordapp.net/attachments/741645926447317123/1027493842892632074/unknown.png)

### Autoriser le traffic sur le port qu'utilise `nc`

J'ai créé une nouvelle règle nommée `Netcat` qui permet d'accepter toutes les connexions sur le port 8888 (sans mentionnant le programme `netcat`)

![](https://media.discordapp.net/attachments/741645926447317123/1027494510747451402/unknown.png?width=814&height=662)

*Cette fenêtre fait peur*

![](https://media.discordapp.net/attachments/741645926447317123/1027494951052902410/unknown.png)

*La règle Netcat pour le port TCP 8888 (toujours pas lié au programme `nc` directement)*

Le firewall est maintenant setup correctement

## III. Manipulations d'autres outils/protocoles côté client

### Exploration du DHCP, depuis votre PC

### Afficher l'adresse IP du serveur DHCP du réseau WiFi YNOV

Commande : `ipconfig /all`

Résultat : 
```
   ...

   Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6E AX210 160MHz
   Adresse physique . . . . . . . . . . . : 84-5C-F3-F5-18-F6
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::894b:b9a4:23fa:f93%4(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.17.11(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Bail obtenu. . . . . . . . . . . . . . : jeudi 6 octobre 2022 08:59:30
   Bail expirant. . . . . . . . . . . . . : vendredi 7 octobre 2022 08:59:26
   Passerelle par défaut. . . . . . . . . : 10.33.19.254
   Serveur DHCP . . . . . . . . . . . . . : 10.33.19.254
   IAID DHCPv6 . . . . . . . . . . . : 59006195
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-27-D0-A1-9C-D8-BB-C1-1E-AE-FA
   Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
                                       8.8.4.4
                                       1.1.1.1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé

   ...
```

Adresse du serveur DHCP : `10.33.19.254`

Date d'expiration du bail DHCP : `vendredi 7 octobre 2022 08:59:26`

### DNS

### Trouver l'adresse IP du serveur DNS que connaît votre ordinateur

Commande : `ipconfig /all`

Résultat : 
```
   ...

   Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6E AX210 160MHz
   Adresse physique . . . . . . . . . . . : 84-5C-F3-F5-18-F6
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::894b:b9a4:23fa:f93%4(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.17.11(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Bail obtenu. . . . . . . . . . . . . . : jeudi 6 octobre 2022 08:59:30
   Bail expirant. . . . . . . . . . . . . : vendredi 7 octobre 2022 08:59:26
   Passerelle par défaut. . . . . . . . . : 10.33.19.254
   Serveur DHCP . . . . . . . . . . . . . : 10.33.19.254
   IAID DHCPv6 . . . . . . . . . . . : 59006195
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-27-D0-A1-9C-D8-BB-C1-1E-AE-FA
   Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
                                       8.8.4.4
                                       1.1.1.1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé

   ...
```

Adresse IP du serveur DNS (primaire) : `8.8.8.8`

### Faites un lookup (lookup = "dis moi à quelle IP se trouve tel nom de domaine") pour google.com

Commande : `nslookup google.com`

Résultat :

```
Serveur :   dns.google
Address:  8.8.8.8

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:80f::200e
          142.250.179.110
```

Adresses de google.com : 
 + IPv4: `142.250.179.110`
 + IPv6: `2a00:1450:4007:80f::200e`

### Lookup pour ynov.com

Commande : `nslookup ynov.com`

```
Serveur :   dns.google
Address:  8.8.8.8

Réponse ne faisant pas autorité :
Nom :    ynov.com
Addresses:  2606:4700:20::ac43:4ae2
          2606:4700:20::681a:ae9
          2606:4700:20::681a:be9
          104.26.10.233
          104.26.11.233
          172.67.74.226
```

Adresses de ynov.com : 
 + IPv4: `104.26.10.233`
 + IPv6: `2606:4700:20::ac43:4ae2`

Adresse IP du serveur DNS : `8.8.8.8`

### Reverse lookup de `231.34.113.12`

Commande : `nslookup 231.34.113.12`

Retour :

```
Serveur :   dns.google
Address:  8.8.8.8

*** dns.google ne parvient pas à trouver 231.34.113.12 : Non-existent domain
```

L'adresse IP `231.34.113.12` ne correspond à aucun nom de domaine.

### Reverse lookup de `78.34.2.17`

Commande : `nslookup 78.34.2.17`

Retour :

```
Serveur :   dns.google
Address:  8.8.8.8

Nom :    cable-78-34-2-17.nc.de
Address:  78.34.2.17
```

L'adresse IP `78.34.2.17` correspond au nom de domaine `cable-78-34-2-17.nc.de` sur le DNS de Google.

## Wireshark

## 1. Intro

### Mettre en évidence un `ping` entre la machine et la passerelle.

```
ping 10.33.19.254

Envoi d’une requête 'Ping'  10.33.19.254 avec 32 octets de données :
Délai d’attente de la demande dépassé.

Statistiques Ping pour 10.33.19.254:
Paquets : envoyés = 1, reçus = 0, perdus = 1 (perte 100%)
```

![](https://media.discordapp.net/attachments/741645926447317123/1027516959354523658/unknown.png?width=1440&height=63)

### Netcat
![](https://media.discordapp.net/attachments/741645926447317123/1027521355773648906/unknown.png?width=1440&height=67)

### Requête DNS

![](https://media.discordapp.net/attachments/741645926447317123/1027520925995905095/unknown.png?width=1440&height=44)

Le serveur DNS est à l'adresse : `8.8.8.8`

## 2. Bonus : avant-goût TCP et UDP

### Wireshark it : déterminez à quelle IP et quel port votre PC se connecte quand vous regardez une vidéo Youtube

![](https://media.discordapp.net/attachments/741645926447317123/1027523670085742612/unknown.png?width=1440&height=41)

![](https://media.discordapp.net/attachments/741645926447317123/1027525851769753682/unknown.png?width=1223&height=662)

Adresse IP : `91.68.245.76` (`rr1.sn-n4g-jqber.googlevideo.com`)

Port : `54251` (source) vers `443` (destination)